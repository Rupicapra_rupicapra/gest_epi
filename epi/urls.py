from django.conf.urls import url

from . import views

urlpatterns = [
        url(r'^$', views.index, name='index'),
        url(r'^kit/$', views.kits, name='kits'),
        url(r'^epi/(?P<epi_id>[0-9]+)/$', views.detail_epi, name='detail_epi'),
        url(r'^kit/(?P<kit_id>[0-9]+)/$', views.detail_kit, name='detail_kit'),
        ]

