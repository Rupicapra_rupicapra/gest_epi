# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-08-27 14:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('epi', '0006_inspection'),
    ]

    operations = [
        migrations.AddField(
            model_name='inspection',
            name='e_details',
            field=models.TextField(blank=True, null=True, verbose_name='Détails'),
        ),
    ]
